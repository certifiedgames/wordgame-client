package certifiedgames.wordgame;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String hostName = "10.39.88.41";
        int portNumber = 53244;

        try {
            Socket socket = new Socket(hostName, portNumber);
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            SocketProtocol protocol = new SocketProtocol();
            String fromServer;
            String toSend;

            out.println("name " + promptName());

            while ((fromServer = in.readLine()) != null) {

                toSend = protocol.processInput(fromServer);
                if (toSend != null) {
                    if (toSend.equals("stop")) {
                        break;
                    }

                    out.println(toSend);
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static String promptName() {
        Scanner s = new Scanner(System.in);

        System.out.println("Enter your name:");
        return s.nextLine();
    }
}
