package certifiedgames.wordgame;

import java.util.Scanner;

public class SocketProtocol {

    private static Scanner s = new Scanner(System.in);

    /**
     * Process input received from the socket server. This method will run on the thread of the socket connection.
     *
     * @param input Input received from socket server.
     * @return Text to return to client; return null to return nothing; return `stop` to kill connection.
     */
    public String processInput(String input) {
        String[] args = input.split(" ");

        switch (args[0]) {
            case "challenge":
                System.out.println("You have been challenged!");
                System.out.println("Please enter your target word. This must be a valid dictionary word, and spellable with the current letters.");
                return "response " + s.nextLine();
            case "START":
                System.out.println("You are the first player! Please enter a letter to start with.");
                return "letter " + s.nextLine().charAt(0);
            case "NEXTER":
                System.out.println("It is your turn.");
                System.out.println("Current word: " + args[1]);
                System.out.println("Enter your next letter, or \"challenge\" to challenge");

                String clientInput = s.nextLine();
                if (clientInput.equalsIgnoreCase("challenge")) {
                    return "challenge";
                } else {
                    return "letter " + clientInput.charAt(0);
                }
            case "TURN":
                System.out.println(args[1] + " has played. The word is now " + args[2]);
                break;
            case "VALID":
                System.out.println("Your response was a valid word. You win!");
                break;
            case "INVALID":
                System.out.println("That wasn't a valid word. You lose.");
                break;
            case "VALIDOTHER":
                System.out.println("The challenge was successful.");
                break;
            case "INVALIDOTHER":
                System.out.println("The challenge was unsuccessful.");
        }
        return null;
    }
}
